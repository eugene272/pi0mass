#include <iostream>
#include "fTreeQA.C"

using namespace std;

int main(int argc, char** argv)
{
    if (argc < 2) {
        cerr << "No input file specified" << endl;
        return 1;
    }

    TFile* fd = new TFile(argv[1]);
    if (!fd->IsOpen()) {
        return 1;
    } 

    fTreeQA f((TTree*) fd->Get("fTreeQA"));
    f.Loop();

    return 0;
}


